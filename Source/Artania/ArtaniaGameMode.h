// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArtaniaGameMode.generated.h"

UCLASS(minimalapi)
class AArtaniaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AArtaniaGameMode();
};



