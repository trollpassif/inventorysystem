
#pragma once

//Input
#include "InputCoreTypes.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

UCLASS(config = Game)
class ARTANIA_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AYourPlayerController(const FObjectInitializer& ObjectInitializer);
};